

all: bin/main

bin/main: main.cpp
	g++ main.cpp  -o main -lsfml-graphics -lsfml-window -lsfml-system

run:
	main

clear:
	rm -rf bin/main
	
