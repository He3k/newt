#include <SFML/Graphics.hpp>
#include <time.h>
#include <iostream>

using namespace sf;

const int L = 10;///  �����
const int W = 20;////  ������

int field[W][L] = { 0 };//����
int s = 34;///������ ������


struct Point {
	int x, y;
} a[4], b[4];

int figur[7][4] = {///������ � ���� �������
	1,3,5,7, ///1 ������ 
	2,3,4,5, ///2 ������ � �.�.
	3,4,5,7,
	3,4,5,6,
	3,4,5,7,
	2,4,6,7,
	3,5,6,7
};//������ � ���� �������

bool check() {//�������� �� ����������� ����������� �� ������ ����
	for (int i = 0; i < 4; i++)
	{
		if ((a[i].x < 0) || (a[i].x >= L) || (a[i].y >= W))
			return 0;///���� �� ����
		else if (field[a[i].y][a[i].x])
			return 0;///���� ������ ���
	};
	return 1;
};

int main() {
	srand(time(0));
	int pt = 0;

	RenderWindow window(VideoMode(L * s, W * s), "Game");//�������� ���� ����
	//Texture go;
	Texture sq;
	sq.loadFromFile("2.png");
	Sprite sqs(sq);///�������� ������� (������ �� ������� ������������� ��������) �� ��������� ��������
	//go.loadFromFile("C:/Users/����/Downloads/3.png");
	//Sprite letter(go);
	int var = 1;//������� �����
	int dx = 0;
	bool rotate = false;

	float times = 0;
	float stop = 0.3;
	Clock clock; ///������� ����� ��� ��������� �������

	while (window.isOpen())
	{
		float time = clock.getElapsedTime().asSeconds();///getElapsedTime() ������������ �����, ��������� � ������� �����
		clock.restart();///estart() ��������������� ����
		times += time;

		Event event;
		while (window.pollEvent(event))//��������� ������� ������� ���� �������
		{
			if (event.type == Event::Closed)
			{
				window.close();
			}

			if (event.type == Event::KeyPressed)
				if (event.key.code == Keyboard::Up)
					rotate = true;//������ ��� ����� ����� ������� ������ 
				else if (event.key.code == Keyboard::Left)
					dx = -1;//������ ��� ������ ����� � ������ �������������� ����������� �������
				else if (event.key.code == Keyboard::Right)
					dx = 1;
			if (check() == false)//��������� ���� � ����� �����
			{
				
				//for (int i = 0; i < 8; i++) {//��������� ������ 
					//letter.setTextureRect(IntRect(i * s, 0, s, s));
					//letter.setPosition(a[i].x * s, a[i].y * s);
					//window.draw(letter);
				//}

				std::cout << "Point: " << pt << std::endl;
				window.close();

			}
		}

		if (Keyboard::isKeyPressed(Keyboard::Down))
			stop = 0.05;//��������� ������ ����

		for (int i = 0; i < 4; i++)
			a[i].x += dx;


		for (int i = 0; i < 4; i++) {
			b[i] = a[i];
			a[i].x += dx;
		}

		if (!check()) {
			for (int i = 0; i < 4; i++) {
				a[i] = b[i];
			}
		}

		if (rotate) { //������� 
			Point p = a[1];
			for (int i = 0; i < 4; i++)
			{
				int x = a[i].y - p.y;
				int y = a[i].x - p.x;
				a[i].x = p.x - x;
				a[i].y = p.y + y;
			}

			if (!check()) {
				for (int i = 0; i < 4; i++) {
					a[i] = b[i];
				}
			}

		}

		if (times > stop) {//�������
			for (int i = 0; i < 4; i++) {
				b[i] = a[i];
				a[i].y += 1;
				times = 0;
			}

			if (!check()) {
				for (int i = 0; i < 4; i++) {
					field[b[i].y][b[i].x] = var;
				}
				var = 1 + rand() % 7;//����������� �����
				int n = rand() % 7;//��� ������
				for (int i = 0; i < 4; i++)
				{
					a[i].x = figur[n][i] % 2;
					a[i].y = figur[n][i] / 2;//�������� ������
				}
			}
		}

		int k = W - 1;//������ �������� �� ������� ������������ �����
		for (int i = W - 1; i > 0; i--) {//��������� ���� �� ������ ����������� �����
			int count = 0;
			for (int j = 0; j < L; j++) {
				if (field[i][j])
				{
					count++;
				}//�������� ���������� ������
				field[k][j] = field[i][j];
			}
			if (count < L)
				k--;//��������� ������� ����
			else pt++;//��������� ���� ��� ���������� ������
		}


		dx = 0;
		rotate = false;
		stop = 0.3;

		window.clear(Color::Black);//�������� ����

		for (int i = 0; i < W; i++) {
			for (int j = 0; j < L; j++) {
				if (field[i][j] == 0)
					continue;
				sqs.setTextureRect(IntRect(field[i][j] * s, 0, s, s));
				sqs.setPosition(j * s, i * s);
				window.draw(sqs);
			}
		}

		for (int i = 0; i < 4; i++) {//��������� ������ 
			sqs.setTextureRect(IntRect(var * s, 0, s, s));
			sqs.setPosition(a[i].x * s, a[i].y * s);
			window.draw(sqs);
		}

		window.display();// ����������
	}
	return 0;
}